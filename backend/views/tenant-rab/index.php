<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TenantKegiatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant RAB';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Tenant RAB
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-kegiatan-index">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                'nama_kegiatan',
                                'koordinator_peneliti',
                                // 'judul',
                                // 'tujuan:raw',
                                //'sasaran:raw',
                                //'id_pic',

                                [
                                    'buttons' => [
                                        'view' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['/tenant-rab/rab', 'TenantRabSearch[id_kegiatan]' => $model->id]), ['title' => Yii::t('yii', 'View')]);
                                        },
                                    ],
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}'
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

