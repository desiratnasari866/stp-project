<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BeritaSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Berita
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="berita-search">

                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                        <?= $form->field($model, 'id') ?>

                        <?= $form->field($model, 'judul_berita') ?>

                        <?= $form->field($model, 'sumber_berita') ?>

                        <?= $form->field($model, 'isi_berita') ?>

                        <?= $form->field($model, 'tanggal_publikasi') ?>

                        <?php // echo $form->field($model, 'upload_foto') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

