<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BeritaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Beritas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Berita
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="berita-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Tambah Berita', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                'judul_berita',
                                'sumber_berita',
                                // 'isi_berita:raw',
                                'tanggal_publikasi',
                                 [
                                    'attribute' =>'upload_foto',
                                    'format' => 'raw',
                                    'value' => function ($row) {
                                        return $uploadFoto = Html::img(
                                            $row->getUploadFotoUrl(),
                                            ['style' => 'width:50px; heigth:20px;']
                                        );
                                    }
                                ],
                                //'upload_foto',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

