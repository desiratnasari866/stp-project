<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GaleryStp */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Galery STP
                    </h3>
                </div>
                <div class="panel-body" >
					<div class="galery-stp-form">

					    <?php $form = ActiveForm::begin(); ?>

					    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'gambar')->fileInput() ?>

					    <div class="form-group">
					        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>


