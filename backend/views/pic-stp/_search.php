<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PicStpSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar PIC STP
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="pic-stp-search">

                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                        <?= $form->field($model, 'id') ?>

                        <?= $form->field($model, 'nama_pic') ?>

                        <?= $form->field($model, 'jabatan') ?>

                        <?= $form->field($model, 'tgl_lahir') ?>

                        <?= $form->field($model, 'nip') ?>

                        <?php // echo $form->field($model, 'jenis_kelamin') ?>

                        <?php // echo $form->field($model, 'email') ?>

                        <?php // echo $form->field($model, 'id_user') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

