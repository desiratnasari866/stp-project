<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PicStpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pic Stps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar PIC STP
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="pic-stp-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Create Pic Stp', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                'nama_pic',
                                'jabatan',
                                'tgl_lahir',
                                'nip',
                                'jenis_kelamin',
                                //'email:email',
                                //'id_user',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

