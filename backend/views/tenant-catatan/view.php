<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TenantCatatan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tenant Catatans', 'url' => ['catatan', 'TenantcatatanSearch[id_kegiatan]' => $model->id_kegiatan]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Catatan Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-catatan-view">

                        <h1><?= Html::encode($this->title) ?></h1>

                        <p>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'id_kegiatan',
                                'tanggal',
                                'lokasi',
                                'catatan_pertemuan:ntext',
                                'rencana:ntext',
                                'keterangan',
                            ],
                        ]) ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

