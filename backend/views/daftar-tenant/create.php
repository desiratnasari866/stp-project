<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DaftarTenant */

$this->title = 'Create Daftar Tenant';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daftar-tenant-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
