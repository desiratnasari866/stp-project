<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DaftarTenant */

$this->title = 'Update Daftar Tenant';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daftar-tenant-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
