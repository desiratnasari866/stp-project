<?php

$this->title = 'Surat Pengajuan Pengabdian Masyarakat';
?>
<!DOCTYPE html>
<html>
<head>
	<title><img src="<?php echo Yii::getAlias('@web/images/logo1.png');?> " width='40px'/><img src="<?php echo Yii::getAlias('@web/images/cstp.png');?> " width='80px'/>Laporan Kegiatan STP</title>
</head>
<body>
	<b><h4 align="center">PENDAMPINGAN KEGIATAN</h4></b>
	<b><h4 align="center">CIBINONG SCIENCE AND TECHNOLOGY PARK</h4></b>
	<br/>
	<div class="container-fluid">
		<br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">1.</td>
		 		<td width="95%">Data Kegiatan</td>
		 	</tr>
		</table>
		<table border="0" width="100%">
		 	<tr>
			    <td width="5%"></td>
			    <td width="30%" align="left" valign="top">Nama Kegiatan</td>
				<td width="5%" align="left" valign="top">:</td>
			    <td width="60%"><p align="justify"><?= $tenantKegiatan->nama_kegiatan ?></p></td>
			</tr>
			<tr>
			    <td width="5%"></td>
			    <td width="30%">Koordinator Peneliti</td>
				<td width="5%">:</td>
			    <td width="60%"><?= $tenantKegiatan->koordinator_peneliti ?></td>
		 	</tr>
		 	<tr>
			    <td width="5%"></td>
			    <td width="30%">Pendamping Kegiatan</td>
				<td width="5%">:</td>
			    <td width="60%"><?= $tenantKegiatan->pic->nama_pic ?></td>
		 	</tr>
		</table>
		<br/>
		 <table border="0" width="100%">
		 	<tr>
		 		<td width="5%">2.</td>
		 		<td width="95%">Hasil Monitoring Tahun 2018</td>
		 	</tr>
		 </table>
		 		<table border="0" width="100%">
		 	<tr>
			    <td width="5%"></td>
			    <td width="30%" align="left" valign="top">Sasaran</td>
				<td width="5%" align="left" valign="top">:</td>
			    <td width="60%" align="justify"><?= $tenantKegiatan->sasaran ?></td>
			</tr>
			<tr>
			    <td width="5%"></td>
			    <td width="30%" align="left" valign="top">Tujuan</td>
				<td width="5%" align="left" valign="top">:</td>
			    <td width="60%" align="justify"><?= $tenantKegiatan->tujuan ?></td>
			</tr>
		</table>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">3.</td>
		 		<td width="95%">Anggaran Kegiatan 2018</td>
		 	</tr>
		 </table>
		 <table border="0" width="100%">
		 	<tr>
			    <td width="5%"></td>
			    <td width="95%" align="left" valign="top"><img src="<?= isset($tenantRab) ? $tenantRab->getUploadRab() : '' ?>" width="100%" /></td>
			</tr>
		</table>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">4.</td>
		 		<td width="95%">Milestone Kegiatan Tahun Anggaran 2018</td>
		 	</tr>
		 </table>
		 <br/>
		 <table border="1" cellspacing="0" width="100%">
		 	<tr style="background-color: #9ACD32">
			    <td width="30%" align="left" valign="top">Judul</td>
			    <td width="70%"><?= $tenantKegiatan->judul ?></td>
			</tr>
			<tr style="background-color: #F5DEB3">
			    <td width="30%" align="left" valign="top">Tujuan</td>
			    <td width="70%"><?= $tenantKegiatan->tujuan ?></td>
			</tr>
			<tr style="background-color: #F5DEB3">
			    <td width="30%" align="left" valign="top">Sasaran</td>
			    <td width="70%"><?= $tenantKegiatan->sasaran ?></td>
			</tr>
		</table>
		<table border="1" cellspacing="0" width="100%">
		 	<tr style="background-color: #D3D3D3">
		 		<td width="100%" align="center" valign="top">Sasaran Antara Utama/Key Milestone 2018</td>
			</tr>
		</table>
		<table border="1" cellspacing="0" width="100%">
		 	<tr>
		 		<td width="5%" align="center" valign="top">No</td>
		 		<td width="25%" align="center" valign="top">Bulan</td>
		 		<td width="70%" align="center" valign="top">Keterangan</td>
			</tr>
			<?php if ($tenantKegiatan->tenantMilestones) : ?>
            	<?php foreach ($tenantKegiatan->tenantMilestones as $i => $milestone) : ?>
                  	<tr>
                      	<td width="5%" align="center"><?= $i + 1 ?></td>
				 		<td width="25%" align="center"><?= \Yii::$app->formatter->asDate($milestone->bulan, 'php:Y-F') ?></td>
				 		<td width="70%" align="justify" valign="top"><?= $milestone->keterangan ?></td>
			 		</tr>
			 	<?php endforeach ?>
            <?php endif ?>
		</table>
		<table border="1" cellspacing="0" width="100%">
		 	<tr>
		 		<td width="100%" align="justify" valign="top">Catatan</td>
			</tr>
			<tr>
		 		<td width="100%" align="justify" valign="top">1.</td>
			</tr>
			<tr>
		 		<td width="100%" align="justify" valign="top">2.</td>
			</tr>
		</table>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">5.</td>
		 		<td width="95%">Kolom Catatan/Rangkuman Pendampingan</td>
		 	</tr>
		 </table>
		 <br/>
		 <table border="1" cellspacing="0" width="100%">
		 	<tr>
		 		<td width="5%" align="center" valign="top">No</td>
		 		<td width="15%" align="center" valign="top">Waktu</td>
		 		<td width="15%" align="center" valign="top">Lokasi</td>
		 		<td width="25%" align="center" valign="top">Catatan Pertemuan/Sebelumnya</td>
		 		<td width="25%" align="center" valign="top">Rencana/Tidak Lanjut</td>
		 		<td width="10%" align="center" valign="top">Keterangan</td>
			</tr>
			<?php if ($tenantKegiatan->tenantCatatans) : ?>
            	<?php foreach ($tenantKegiatan->tenantCatatans as $i => $catatan) : ?>
                    <tr>
				 		<td width="5%" align="center" valign="top"><?= $i + 1 ?></td>
				 		<td width="15%" align="center" valign="top"><?= $catatan->tanggal ?></td>
				 		<td width="15%" align="center" valign="top"><?= $catatan->lokasi ?></td>
				 		<td width="25%" align="center" valign="top"><?= $catatan->catatan_pertemuan ?></td>
				 		<td width="25%" align="center" valign="top"><?= $catatan->rencana ?></td>
				 		<td width="10%" align="center" valign="top"><?= $catatan->keterangan ?></td>
				 	</tr>
				<?php endforeach ?>
            <?php endif ?>
		</table>
		 <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		 <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">6.</td>
		 		<td width="95%">Log Book Penyerapan Anggaran</td>
		 	</tr>
		 </table>
		 <br/>
		 <table border="1" cellspacing="0" width="100%">
		 	<tr>
		 		<td width="5%" align="center" valign="top">No</td>
		 		<td width="40%" align="center" valign="top">Jenis Belanja</td>
		 		<td width="25%" align="center" valign="top">Nominal</td>
		 		<td width="15%" align="center" valign="top">Tanggal Pengajuan</td>
		 		<td width="15%" align="center" valign="top">Tanggal Pencairan</td>
			</tr>
			<?php if ($tenantKegiatan->tenantLogbooks) : ?>
            	<?php foreach ($tenantKegiatan->tenantLogbooks as $i => $logbook) : ?>
                    <tr>
				 		<td width="5%" align="center" valign="top"><?= $i + 1 ?></td>
				 		<td width="40%" align="justify" valign="top"><?= $logbook->jenis_belanja ?></td>
				 		<td width="25%" align="center" valign="top"><?= $logbook->nominal ?></td>
				 		<td width="15%" align="center" valign="top"><?= $logbook->tanggal_pengajuan ?></td>
				 		<td width="15%" align="center" valign="top"><?= $logbook->tanggal_pencairan ?></td>
				 	</tr>
				<?php endforeach ?>
            <?php endif ?>
		</table>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		 <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<table border="0" width="100%">
		 	<tr>
		 		<td width="5%">7.</td>
		 		<td width="95%">Dokumentasi Pendampingan</td>
		 	</tr>
		 </table>
		 <br/>
		 <table border="1" cellspacing="0" width="100%">
		 	<tr>
		 		<td width="5%" align="center" valign="top">No</td>
		 		<td width="45%" align="center" valign="top">Nama Kegiatan</td>
		 		<td width="50%" align="center" valign="top">Foto</td>
			</tr>
			<?php if ($tenantKegiatan->tenantDokumentasis) : ?>
            	<?php foreach ($tenantKegiatan->tenantDokumentasis as $i => $dokumentasi) : ?>
                    <tr>
				 		<td width="5%" align="center" valign="top"><?= $i + 1 ?></td>
				 		<td width="45%" align="center"><?= $dokumentasi->nama_kegiatan ?></td>
				 		<td width="50%" align="center" valign="top"><img src="<?= $dokumentasi->getUploadFoto() ?>" width="10%" /></td>
				 	</tr>
				<?php endforeach ?>
            <?php endif ?>
		</table>
	</div>
</body>
</html>