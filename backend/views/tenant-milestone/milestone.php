<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TenantMilestone;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TenantMilestoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant Milestones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Milestone Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-milestone-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Create Tenant Milestone', ['create', 'id_kegiatan' => \Yii::$app->request->get('TenantMilestoneSearch')['id_kegiatan']], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                // 'id_kegiatan',
                                // [
                                //     'label' => 'bulan',
                                //     'format'=> 'raw',
                                //     'value' => function($model)
                                //     {
                                //         return TenantMilestone::bulan($model->bulan);
                                //     }
                                // ],
                                [
                                    'attribute' => 'bulan',
                                    'format' => ['date', 'php:Y-F'],
                                ],
                                'keterangan:raw',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

