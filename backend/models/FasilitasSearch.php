<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Fasilitas;

/**
 * FasilitasSearch represents the model behind the search form of `common\models\Fasilitas`.
 */
class FasilitasSearch extends Fasilitas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_fasilitas', 'isi_fasilitas', 'upload_gambar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fasilitas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nama_fasilitas', $this->nama_fasilitas])
            ->andFilterWhere(['like', 'isi_fasilitas', $this->isi_fasilitas])
            ->andFilterWhere(['like', 'upload_gambar', $this->upload_gambar]);

        return $dataProvider;
    }
}
