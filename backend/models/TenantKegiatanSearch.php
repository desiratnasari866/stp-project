<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TenantKegiatan;

/**
 * TenantKegiatanSearch represents the model behind the search form of `common\models\TenantKegiatan`.
 */
class TenantKegiatanSearch extends TenantKegiatan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_pic'], 'integer'],
            [['nama_kegiatan', 'koordinator_peneliti', 'judul', 'tujuan', 'sasaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantKegiatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_pic' => $this->id_pic,
        ]);

        $query->andFilterWhere(['like', 'nama_kegiatan', $this->nama_kegiatan])
            ->andFilterWhere(['like', 'koordinator_peneliti', $this->koordinator_peneliti])
            ->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'tujuan', $this->tujuan])
            ->andFilterWhere(['like', 'sasaran', $this->sasaran]);

        if (! Yii::$app->user->can('backend_tenant_kegiatan_all')) {
            $query->andWhere(['id_pic' => Yii::$app->user->id]);
        }

        return $dataProvider;
    }
}
