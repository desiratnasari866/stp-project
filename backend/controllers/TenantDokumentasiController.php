<?php

namespace backend\controllers;

use Yii;
use common\models\TenantDokumentasi;
use backend\models\TenantDokumentasiSearch;
use common\models\TenantKegiatan;
use backend\models\TenantKegiatanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TenantDokumentasiController implements the CRUD actions for TenantDokumentasi model.
 */
class TenantDokumentasiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TenantDokumentasi models.
     * @return mixed
     */
    public function actionDokumentasi()
    {
        $this->layout = 'admin';
        $searchModel = new TenantDokumentasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('dokumentasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $this->layout = 'admin';
        $searchModel = new TenantKegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('Index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TenantDokumentasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TenantDokumentasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new TenantDokumentasi();
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/tenant-dokumentasi/';

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'gambar');
            $model->upload_foto = $img->name;
            $model->gambar = $img;
            $model->save();
            $model->gambar->saveAs(Yii::$app->params['uploadPath'] . $model->gambar);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TenantDokumentasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/tenant-dokumentasi/';

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'gambar');
            $model->upload_foto = $img->name;
            $model->gambar = $img;
            $model->save();
            $model->gambar->saveAs(Yii::$app->params['uploadPath'] . $model->gambar);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TenantDokumentasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout = 'admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TenantDokumentasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TenantDokumentasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenantDokumentasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
