<?php

namespace backend\controllers;

use backend\models\TenantKegiatanSearch;
use common\models\TenantKegiatan;
use common\models\TenantRab;
use common\models\TenantMilestone;
use common\models\TenantLogbook;
use common\models\TenantCatatan;
use common\models\TenantDokumentasi;
use common\models\PicStp;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;



/**
 * Site controller
 */
class DefaultController extends Controller
{
	public function actionIndex()
    {
        $this->layout = 'admin';
        $searchModel = new TenantKegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport($id)  // $id = id_tenant_kegiatan
    {
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('report', [
            'tenantKegiatan' => TenantKegiatan::findOne($id),
            // select * from tenant_rab where id_tenant_kegiatan = $id
            'tenantRab' => TenantRab::find()->Where(['id_kegiatan' => $id])->one(),
            'tenantMilestone' => TenantMilestone::find()->Where(['id_kegiatan' => $id])->one(),
            'tenantLogbook' => TenantLogbook::find()->Where(['id_kegiatan' => $id])->one(),
            'tenantDokumentasi' => TenantDokumentasi::find()->Where(['id_kegiatan' => $id])->one(),
            'tenantCatatan' => TenantCatatan::find()->Where(['id_kegiatan' => $id])->one(),
        ]);//parameter pertama view, parameter ke 2 datanya
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Laporan Pendampingan Kegiatan STP'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['INDUSTRI TENANT CSTP'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
             // http response
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_RAW;
            $headers = Yii::$app->response->headers;
            $headers->add('Content-Type', 'application/pdf');
            // return the pdf output as per the destination setting
            return $pdf->render(); 
    }
}