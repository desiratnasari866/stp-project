<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Konsep STP';
$this->params['breadcrumbs'][] = $this->title;
$kegiatanStp = \common\models\Kegiatan::find()->one();
?>
 <img src="<?php echo Yii::getAlias('@web/images/home.png');?> " alt="Post" width="100%"/>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Kegiatan STP</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <section class="category-content col-sm-9">
                    <h2 class="category-title">KEGIATAN STP</h2>
                    <br/>
                    <p><?= $kegiatanStp->isi_kegiatan ?></p>
                </section>
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>MENU</h4>
                        <ul>
                            <li class="current"><a href="/yii/site/about" title="">Science Techno Park</a></li>
                            <li><a href="/yii/site/konsepstp" title="">Konsep STP</a></li>
                            <li><a href="/yii/site/kegiatanstp" title="">Kegiatan STP</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </main>