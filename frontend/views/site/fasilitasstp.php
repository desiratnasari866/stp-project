<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Fasilitas';
$this->params['breadcrumbs'][] = $this->title;
?>
 <img src="<?php echo Yii::getAlias('@web/images/cstp.png');?> " alt="Post" width="100px"/>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Fasilitas</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>FASILITAS STP</h4>
                        <ul>
                            <?php if ($fasilitass = \common\models\Fasilitas::find()->all()) : ?>
                            <?php foreach ($fasilitass as $i => $fasilitas) : ?>
                            <li><a href="#" title=""><?= $fasilitas->nama_fasilitas ?></a></li>
                           <?php endforeach ?>
                        <?php endif ?>
                        </ul>
                    </div>
                </aside>
                 <section class="category-content col-sm-9">
                    <div class="widget">
                        <h2 class="category-title">FASILITAS</h2>
                        <P align="justify">C-STP LIPI facilitates and synergizes elements of regional innovation system in nurturing and developing
                        knowledge based industry with the main objectives to enhance industry competitiveness and to improve productivity of the sosiety.</P>
                    </div>
                </section>
            </div>
        </div>
    </main>