<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Daftar Tenant';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bread_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="/yii" title="Post">Home</a></li>
                    <li class="active">Daftar Tenant</li>
                </ol>
            </div>
        </div>
    </div>
</div>
    <main class="site-main page-main">
        <div class="container">
            <div class="row">
    <section class="page col-sm-9">
        <h2 class="page-title">DAFTAR TENANT CSTP</h2>
    </section>
    <section class="services">
    <div class="container">
        <div class="row">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'daftartenant_list',
                'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
            ]) ?>
        </div>
    </div>
</section>
</div>
</div>
</main>
