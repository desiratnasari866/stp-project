<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
$profileStp = \common\models\ProfileStp::find()->one();
?>
 <img src="<?php echo Yii::getAlias('@web/images/home.png');?> " alt="Post" width="100%"/>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Profile</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <main class="site-main page-main">
        <div class="container">
            <div class="row">
                <section class="page col-sm-9">
                    <h2 class="page-title">PROFILE CSTP</h2>
                    <div class="entry"><font align="justify"><?= $profileStp->latar_belakang ?></font></div>
                </section>
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>MENU</h4>
                        <ul>
                            <li class="current"><a href="/yii/site/about" title="">Science Techno Park</a></li>
                            <li><a href="/yii/site/konsepstp" title="">Konsep STP</a></li>
                            <li><a href="/yii/site/kegiatanstp" title="">Kegiatan STP</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
            <section class="services">
            <div class="row">
                <section class="page col-sm-12">
                    <h2 class="page-title">VISI DAN MISI</h2>
                </section>
            </div>
            <section class="boxes_area" style="width: 1195px">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <h3>VISI</h3>
                                <p><?= $profileStp->visi ?></p>
                                <i class="fa fa-line-chart"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box">
                                <h3>VISI</h3>
                                <p><?= $profileStp->misi ?></p>
                                <i class="fa fa-list-alt"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        </div>
    </main>