<?php

use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Home';
$profileStp = \common\models\ProfileStp::find()->one();
$kegiatan = \common\models\Kegiatan::find()->one();
$konsepStp = \common\models\KonsepStp::find()->one();
?>
    <main class="site-main">
        <section class="hero_area"  style="background-image: url(/yii/images/slidehome.jpg)" width="20px;" >
        </section>
        <!-- <video src="/yii/frontend/web/video/tes.mp4" controls></video> -->
        <section class="boxes_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box">
                            <h3><a href="/yii/site/konsepstp">Konsep STP</a></h3>
                            <p><?= \yii\helpers\StringHelper::truncate($konsepStp->isi_konsep, 200) ?></p>
                            <i class="fa fa-cogs"></i>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box">
                            <h3><a href="/yii/site/kegiatanstp">Kegiatan STP</a></h3>
                            <p><?= \yii\helpers\StringHelper::truncate($kegiatan->isi_kegiatan, 200) ?></p>
                            <i class="fa fa-line-chart"></i>
                        </div>
                    </div>
                    <div class="col-sm-4"> 
                        <div class="box">
                            <h3><a href="/yii/site/about">Pofile STP</a></h3>
                            <font align="justify"><p><?= \yii\helpers\StringHelper::truncate($profileStp->latar_belakang, 200) ?></p></font>
                            <i class="fa fa-home"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services">
            <h2 class="section-title">SERVICES</h2>
            <!-- <p class="desc">Praesent faucibus ipsum at sodales blandit</p> -->
            <div class="container">
                <div class="row">
                    <?php if ($services = \common\models\Service::find()->all()) : ?>
                        <?php foreach ($services as $i => $service) : ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <i class="fa fa-cogs"></i>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?= $service->nama_service ?></h4>
                                        <p><?= $service->detail_service ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </section>
        <section class="home-area">
            <div class="home_content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12"><h2 class="sub_title">LATEST NEWS</h2></div>
                        <div class="home_list">
                            <ul>
                                <?php if ($beritas = \common\models\Berita::find()->all()) : ?>
                                    <?php foreach ($beritas as $i => $berita) : ?>
                                         <li class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="thumbnail">
                                                <img alt="<?= $berita->judul_berita ?>" src="<?= $berita->getUploadFoto() ?>" width="300px" />
                                                <div class="caption">
                                                    <p><?= $berita->tanggal_publikasi ?></p>
                                                    <h3><a href="<?= Url::to(['berita/detail', 'id' => $berita->id]) ?>" title="Post Title"><?= $berita->judul_berita ?></a></h3>
                                                    <font align="justify"><p><?= \yii\helpers\StringHelper::truncate($berita->isi_berita, 200) ?></p></font>
                                                    <a href="#" class="btn btn-link" role="button">More</a>
                                                </div>
                                            </div>                                        
                                        </li>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </ul>
                        </div>
                        
                        <div class="col-sm-12 home_bottom">
                            <h2 class="sub_title">DAFTAR TENANT</h2>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="6000" id="myCarousel">
                                    <div class="carousel-inner">
                                        <?php if ($daftarTenants = \common\models\DaftarTenant::find()->all()) : ?>
                                            <?php foreach ($daftarTenants as $i => $tenant) : ?>
                                                <div class="item <?= $i == 0 ? 'active' : '' ?>">
                                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                                        <a href="#">
                                                            <img alt="<?= $tenant->nama_tenant ?>" class="img-responsive" src="<?= $tenant->getLogo() ?>" width="150px" />
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>