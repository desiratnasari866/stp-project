<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\HomeAsset;
use common\widgets\Alert;

HomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<!-- <body style="background-image: url(/yii/frontend/web/images/slidehome.jpg)" width="20px;"> -->
<body>
<?php $this->beginBody() ?>
    <header class="site-header">
        <div class="top" style="background-color: #000080 ">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Cibinong Science Techno Park</p>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-inline pull-right">
                            <li><a href="https://www.facebook.com/inovasiLIPI"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/inovasiLIPI"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/company-beta/822507"><i class="fa fa-linkedin"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-envelope-o"></i></a></li> -->
                            <li><a href="tel:(+62) 21-8791-7216"><i class="fa fa-phone"></i> (+62) 21-8791-7216</a></li>
                        </ul>                        
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <i class="fa fa-bars"></i>
                </button>
                <img src="<?php echo Yii::getAlias('@web/images/logo1.png');?> " alt="Post" width="50px"/>
                <img src="<?php echo Yii::getAlias('@web/images/cstp.png');?> " alt="Post" width="100px"/>
                    <!-- <img src="img/logo.png" alt="Post"> -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-navbar-collapse">
                    <ul class="nav navbar-nav main-navbar-nav">
                        <!-- <li class="active"><a href="/yii/frontend/web/site/index" title="">HOME</a></li> -->
                        <li class="active"><?= Html::a(Yii::t('app', 'HOME'), ['/site/index']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'PROFILE'), ['/site/about']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'SERVICE'), ['/site/service']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'BERITA'), ['/site/berita']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'FASILITAS'), ['/site/fasilitas']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'DAFTAR TENANT'), ['/site/daftartenant']) ?></li>
                        <li><?= Html::a(Yii::t('app', 'GALERY'), ['/site/dokumentasi']) ?></li>
                       <!--  <li><a href="/yii/frontend/web/site/about" title="">PROFILE</a></li>
                        <li><a href="/yii/frontend/web/site/service" title="">SERVICE</a></li>
                        <li><a href="/yii/frontend/web/site/berita" title="">BERITA</a></li>
                        <li><a href="/yii/frontend/web/site/fasilitas" title="">FASILITAS</a></li>
                        <li><a href="/yii/frontend/web/site/daftartenant" title="">DAFTAR TENANT</a></li>
                        <li><a href="/yii/frontend/web/site/dokumentasi" title="">GALERY</a></li> -->
                        <li>
                            <?php if (Yii::$app->user->isGuest) : ?>
                                <a href="<?= Yii::$app->urlManagerBackend->createUrl(['/site/login']) ?>">LOGIN</a>
                            <?php else : ?>
                                <?= Html::beginForm(['/site/logout'], 'post') ?>
                                <?= Html::submitButton(
                                    'LOGOUT (' . Yii::$app->user->identity->username . ')',
                                    ['class' => 'btn btn-link logout']
                                ) ?>
                                <?= Html::endForm() ?>
                            <?php endif ?>
                        </li>
                        <li>
                    </ul>                           
                </div><!-- /.navbar-collapse -->                
                <!-- END MAIN NAVIGATION -->
            </div>
        </nav>        
    </header>
    <section class="content">
        <?= $content; ?>
    </section>
    <footer class="site-footer" style="background-color: #000080 ">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 fbox">
                    <font color="white"><h3>ALAMAT</h3></font>
                    <font color="white"><p class="fa fa-home">&emsp;Gedung Pusat Inovasi LIPI</p></font>
                    <a href="https://www.google.com/maps?ll=-6.496912,106.845142&spn=0.006343,0.010568&cid=2069250414098896433&t=m&z=17&iwloc=A">
                        <font color="white"><p class="fa fa-map-signs">&emsp;Jalan Raya Jakarta - Bogor KM. 47 <br/> &emsp;&emsp;Cibinong Maps atau Google Maps</p></font>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 fbox">
                    <font color="white"><h3>HUBUNGI KAMI</h3></font>
                    <font color="white"><p class="fa fa-phone">&emsp;(+62) 21-8791-7216</p></font>
                    <br/>
                    <font color="white"><p class="fa fa-phone">&emsp;(+62) 21-8791-7221</p></font>
                    <br/>
                    <font color="white"><p class="fa fa-envelope-o">&emsp;cstplipi2015@gmail.com</p></font>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 fbox">
                    <font color="white"><h3>MEDIA SOSIAL</h3></font>
                        <a href="https://www.facebook.com/inovasiLIPI"><i class="fa fa-facebook" style="color: white;"></i></a>&emsp;&emsp;
                        <a href="https://twitter.com/inovasiLIPI"><i class="fa fa-twitter" style="color: white;"></i></a>&emsp;&emsp;
                        <a href="https://www.instagram.com/inovasiLIPI/"><i class="fa fa-instagram" style="color: white;"></i></a>&emsp;&emsp;
                        <a href="https://www.youtube.com/lipiindonesia"><i class="fa fa-youtube-play" style="color: white;"></i></a>&emsp;&emsp;
                        <a href="https://www.linkedin.com/company-beta/822507"><i class="fa fa-linkedin" style="color: white;"></i></a>
                </div>
            </div>
        </div>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p align="center">&copy; 2016 COMPANY NAME</p>
                    </div>
                </div>
            </div>
        </div>        
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
