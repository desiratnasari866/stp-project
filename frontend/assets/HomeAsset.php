<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        'https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext',
        'home/css/bootstrap.min.css',
        'home/css/font-awesome.min.css',
        'home/style.css',
        'css/img.css',
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
        'home/js/bootstrap.min.js',
        'home/js/text.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
