<?php

namespace frontend\controllers;

use common\models\Fasilitas;

class FasilitasController extends \yii\web\Controller
{
    public function actionDetail($id)
    {
    	$this->layout = 'home';
    	$data['fasilitas'] = Fasilitas::findOne($id);
        return $this->render('detail', $data);
    }

}
