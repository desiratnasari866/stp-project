<?php

use \kartik\datecontrol\Module;


return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManagerBackend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://localhost/yii/backend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://localhost/yii',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'modules' => [
        'datecontrol' =>  [
            'class' => 'kartik\datecontrol\Module',

            'ajaxConversion' => true,
            'autoWidget' => true, // automatically use kartik\widgets for each of the above formats
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // example
                Module::FORMAT_DATETIME => ['pluginOptions' => ['autoclose' => true]], // setup if needed
                Module::FORMAT_TIME => [], // setup if needed
            ],
            // format settings for displaying each date attribute
            'displaySettings' => [
                Module::FORMAT_DATE => 'php:d-m-Y',
                Module::FORMAT_TIME => 'H:i:s',
                Module::FORMAT_DATETIME => 'php:d-m-Y H:i:s',
            ],
            'displayTimezone' => 'Asia/Jakarta',
            // format settings for saving each date attribute
            'saveSettings' => [
                Module::FORMAT_DATE  => 'php:Y-m-d',
                Module::FORMAT_TIME => 'H:i:s',
                Module::FORMAT_DATETIME  => 'php:Y-m-d H:i:s',
            ],
            'saveTimezone' => 'UTC',
        ],
    ],
];
