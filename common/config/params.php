<?php
return [
    'adminEmail' => 'admin@example.com',
    'backendImagesUrl' => 'http://localhost/stp-project/backend/web/images',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
