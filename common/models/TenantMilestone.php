<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tenant_milestone".
 *
 * @property int $id
 * @property int $id_kegiatan
 * @property string $bulan
 * @property string $keterangan
 *
 * @property TenantKegiatan $kegiatan
 */
class TenantMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kegiatan', 'bulan', 'keterangan'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['bulan'], 'safe'],
            [['keterangan'], 'string', 'max' => 300],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => TenantKegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
        ];
    }

    // public static function bulan($id)
    // {
    //     $ar = array (1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober',
    //     'November', 'Desember');

    //     return $ar[intval($id)];
    // }

    // public static function lapBulan()
    // {
    //     return $ar = array(1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni', 7=>'Juli', 8=>'Agustus', 9=>'September', 
    //         10=>'Oktober', 11=>'November', 12=>'Desember');
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kegiatan' => 'Id Kegiatan',
            'bulan' => 'Bulan',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(TenantKegiatan::className(), ['id' => 'id_kegiatan']);
    }
}
