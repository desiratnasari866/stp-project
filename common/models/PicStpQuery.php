<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PicStp]].
 *
 * @see PicStp
 */
class PicStpQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PicStp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PicStp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function isSelf()
    {
        if (\Yii::$app->user->can('backend_pic_stp_all')) {
            return $this;
        }

        return $this->andWhere(['id_user' => \Yii::$app->user->id]);
    }
}
