<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "galery_stp".
 *
 * @property int $id
 * @property string $foto
 * @property string $nama
 */
class GaleryStp extends \yii\db\ActiveRecord
{
    public $gambar;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'galery_stp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foto', 'nama'], 'required'],
            [['foto'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 300],
            [['gambar'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'foto' => 'Foto',
            'nama' => 'Nama',
        ];
    }

    public function getFoto()
    {
        return \Yii::$app->params['backendImagesUrl'].'/image-galery/'.$this->foto;
    }

    public function getFotoUrl()
    {
        return $this->foto ? Url::to('@web/images/image-galery/'.$this->foto) : '';
    }
}
