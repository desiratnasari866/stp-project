<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pic_stp".
 *
 * @property int $id
 * @property string $nama_pic
 * @property string $jabatan
 * @property string $tgl_lahir
 * @property string $nip
 * @property string $jenis_kelamin
 * @property string $email
 * @property int $id_user
 *
 * @property TenantKegiatan[] $tenantKegiatans
 */
class PicStp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pic_stp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_pic', 'jabatan', 'tgl_lahir', 'nip', 'jenis_kelamin', 'email', 'id_user'], 'required'],
            [['tgl_lahir'], 'safe'],
            [['id_user'], 'integer'],
            [['nama_pic', 'jabatan', 'nip', 'jenis_kelamin', 'email'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pic' => 'Nama Pic',
            'jabatan' => 'Jabatan',
            'tgl_lahir' => 'Tgl Lahir',
            'nip' => 'Nip',
            'jenis_kelamin' => 'Jenis Kelamin',
            'email' => 'Email',
            'id_user' => 'Id User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantKegiatans()
    {
        return $this->hasMany(TenantKegiatan::className(), ['id_pic' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PicStpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PicStpQuery(get_called_class());
    }
}
