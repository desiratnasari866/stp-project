<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
/**
 * This is the model class for table "fasilitas".
 *
 * @property int $id
 * @property string $nama_fasilitas
 * @property string $isi_fasilitas
 * @property string $upload_gambar
 */
class Fasilitas extends \yii\db\ActiveRecord
{
    public $gambar;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fasilitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fasilitas', 'isi_fasilitas', 'upload_gambar'], 'required'],
            [['isi_fasilitas'], 'string'],
            [['nama_fasilitas', 'upload_gambar'], 'string', 'max' => 200],
            [['gambar'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_fasilitas' => 'Nama Fasilitas',
            'isi_fasilitas' => 'Isi Fasilitas',
            'upload_gambar' => 'Upload Gambar',
        ];
    }

    public function getUploadGambar()
    {
        return \Yii::$app->params['backendImagesUrl'].'/image-fasilitas/'.$this->upload_gambar;
    }

    public function getUploadGambarUrl()
    {
        return $this->upload_gambar ? Url::to('@web/images/image-fasilitas/'.$this->upload_gambar) : '';
    }
}
