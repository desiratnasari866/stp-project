<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TenantKegiatan]].
 *
 * @see TenantKegiatan
 */
class TenantKegiatanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TenantKegiatan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TenantKegiatan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function isSelf()
    {
        if (\Yii::$app->user->can('backend_tenant_kegiatan_all')) {
            return $this;
        }

        return $this->andWhere(['id_user' => \Yii::$app->user->id]);
    }
}
