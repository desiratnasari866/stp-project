<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * This is the model class for table "tenant_dokumentasi".
 *
 * @property int $id
 * @property int $id_kegiatan
 * @property string $nama_kegiatan
 * @property string $upload_foto
 *
 * @property TenantKegiatan $kegiatan
 */
class TenantDokumentasi extends \yii\db\ActiveRecord
{
    public $gambar;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_dokumentasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kegiatan', 'nama_kegiatan', 'upload_foto'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['nama_kegiatan', 'upload_foto'], 'string', 'max' => 300],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => TenantKegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
            [['gambar'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kegiatan' => 'Id Kegiatan',
            'nama_kegiatan' => 'Nama Kegiatan',
            'upload_foto' => 'Upload Foto',
        ];
    }

    public function getUploadFotoUrl()
    {
        return $this->upload_foto ? Url::to('@web/images/tenant-dokumentasi/'.$this->upload_foto) : '';
    }

    public function getUploadFoto()
    {
        return \Yii::$app->params['backendImagesUrl'].'/tenant-dokumentasi/'.$this->upload_foto;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(TenantKegiatan::className(), ['id' => 'id_kegiatan']);
    }
}
