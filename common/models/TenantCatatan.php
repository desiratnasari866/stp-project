<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tenant_catatan".
 *
 * @property int $id
 * @property int $id_kegiatan
 * @property string $tanggal
 * @property string $lokasi
 * @property string $catatan_pertemuan
 * @property string $rencana
 * @property string $keterangan
 *
 * @property TenantKegiatan $kegiatan
 */
class TenantCatatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_catatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kegiatan', 'tanggal', 'lokasi', 'catatan_pertemuan', 'rencana', 'keterangan'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['tanggal'], 'safe'],
            [['catatan_pertemuan', 'rencana'], 'string'],
            [['lokasi', 'keterangan'], 'string', 'max' => 300],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => TenantKegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kegiatan' => 'Id Kegiatan',
            'tanggal' => 'Tanggal',
            'lokasi' => 'Lokasi',
            'catatan_pertemuan' => 'Catatan Pertemuan',
            'rencana' => 'Rencana',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(TenantKegiatan::className(), ['id' => 'id_kegiatan']);
    }
}
